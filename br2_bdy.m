function [res] = br2_bdy(nelem, h, ndegr, fpoin, funkno, gpoin, weigh, ...
    basis, dbasis, mass_matrix, mu)
%BR2_BDY build the residual due to the domain integral from BR2
%   
    res = zeros(size(funkno));
    % fac neighbors (periodic)
    elL_idx = [nelem, 1:(nelem - 1)];
    elR_idx = [2:nelem, 1];

    % reference domain face locations
    xiL = -1;
    xiR = 1;

    % flux function
    fvisc = @(u, dudx) -mu * dudx;

    %Element Properties
    Jdet = 0.5 * h;

    for ifac = 1:(nelem) %rightmostface is same as leftmost face (periodic)
        u = funkno(elL_idx(ifac), :) * basis(xiR);
        u_plus = funkno(elR_idx(ifac), :) * basis(xiL);
        u0 = 0.5 * (u + u_plus);

        rhs = basis(xiR) * (u0 - u) * (1);
        lliftcoeff = (mass_matrix \ rhs) / Jdet;
        rhs = basis(xiL) * (u0 - u_plus) * (1);
        llift_pluscoeff = (mass_matrix \ rhs) / Jdet;

        % calculate viscous flux
        llift = lliftcoeff' * basis(xiR);
        llift_plus = llift_pluscoeff' * basis(xiL);
        dudx = funkno(elL_idx(ifac), :) * dbasis(xiR) / Jdet;
        dudx_plus = funkno(elR_idx(ifac), :) * dbasis(xiL) / Jdet;
        f = fvisc(u, dudx + llift);
        f_plus = fvisc(u_plus, dudx_plus + llift_plus);
        flux = 0.5 * (f + f_plus);

        % scatter to element residuals
        res(elL_idx(ifac), :) = res(elL_idx(ifac), :) - flux * basis(xiR)';
        res(elR_idx(ifac), :) = res(elR_idx(ifac), :) + flux * basis(xiL)';
    end
end

