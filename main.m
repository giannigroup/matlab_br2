%% 
% ============================
% = Main file for simulation =
% ============================
%
% This is a simple 1D DG code with:
%  - an evenly spaced mesh
%  - periodic boundary conditions
%  - nodal basis functions
% for debugging and mock-up purposes
clear; clc;

%% Domain Setup
xstart = 0;                                                                 % Start of the domain
xend = 2 * pi;                                                              % End of the domain
nelem = 8;                                                                  % number of elements
ndegr  = 2;                                                                 % the number of dofs per element (minimum 2)
nodes = linspace(xstart, xend, nelem + 1);                                  % Endpoints of the elements
fpoin = zeros(nelem, ndegr);
for el = 1:nelem
    fpoin(el, :) = linspace(nodes(el), nodes(el + 1), ndegr);
end
h = nodes(2) - nodes(1);                                                    % element spacing


%% Physical Properties
mu = 1;

%% Projection
IC = @(x) sin(x);
%IC = @(x) ones(size(x));
funkno = IC(fpoin);                                                         % using nodal basis functions

% plot the initial condition
%figure;
%plotdg(fpoin, funkno);


%% Quadrature Definition
[gpoin, weigh] = get_quadrature(ndegr);

%% Basis function Selection
basis = @lagrangep1;
dbasis = @dlagrangep1;
switch(ndegr)
    case 2
        basis = @lagrangep1;
        dbasis = @dlagrangep1;
    case 3
        basis = @lagrangep2;
        dbasis = @dlagrangep2;
end
mass_matrix = integral( @(x) basis(x) * basis(x)', -1, 1, 'ArrayValued',true);

%% Test br2 domain integral
%res = br2_domn(nelem, h, ndegr, fpoin, funkno, gpoin, weigh, basis, dbasis, mass_matrix, mu);

%% Time stepping
ntime = 8000;
dt = 0.00025;
%ntime = 1;
%dt = 0.1;
for k = 1:ntime
    funkno = funkno + dt * residual(nelem, h, ndegr, fpoin, funkno, gpoin, weigh, basis, dbasis, mass_matrix, mu);
    if (mod(k, 100) == 0)
        clf;
        plotdg(fpoin, funkno);
        pause(0.1);
    end
end

plotdg(fpoin, funkno);
hold on;
xexact = xstart:0.1:xend;
yexact = exp(-ntime * dt) * sin(xexact);
%plot(xexact, yexact);

%% Residual function definition
function [res] = residual(nelem, h, ndegr, fpoin, funkno, gpoin, weigh, basis, dbasis, mass_matrix, mu)
    res = br2_domn(nelem, h, ndegr, fpoin, funkno, gpoin, weigh, basis, dbasis, mass_matrix, mu) + ...
          br2_bdy(nelem, h, ndegr, fpoin, funkno, gpoin, weigh, basis, dbasis, mass_matrix, mu);
end

%% Basis function definitions

function f = lagrangep1(xi)
    f = [ 0.5 * (1 - xi); 0.5 * (xi + 1)];
end

function df = dlagrangep1(xi)
    df = [-0.5; 0.5];
end

% TODO: DOUBLE CHECK ORDER OF BASIS FUNCTIONS ARE THE SAME AS NODAL ORDER
% FOR PROJECITON STEP
function f = lagrangep2(xi)
   f = [ ...
        0.5 * (-xi + xi.^2); ...
        0.5 * (xi + xi.^2); ...
        1 - xi.^2 ...
   ];
end

function df = dlagrangep2(xi)
    df = [...
        xi - 0.5; ...
        xi + 0.5; ...
        -2 * xi ...
    ];
end
