function [res] = br2_domn(nelem, h, ndegr, fpoin, funkno, gpoin, weigh, ...
    basis, dbasis, mass_matrix, mu)
%BR2_DOMN build the residual due to the domain integral from BR2
%   
    res = zeros(size(funkno));
    % element neighbors (periodic)
    elL_idx = [nelem, 1:(nelem - 1)];
    elR_idx = [2:nelem, 1];

    % reference domain face locations
    xiL = -1;
    xiR = 1;

    % flux function
    fvisc = @(u, dudx) -mu * dudx;

    % Element properties
    Jdet = 0.5 * h;

    % calculate global lift operator
    % notation: uplus is the value to the right of the interface
    % all normals point to the right
    for ielem = 1:nelem
        % Left Face glift contrib
        uLminus = funkno(elL_idx(ielem), :) * basis(xiR);
        uLplus = funkno(ielem, :) * basis(xiL); % interior to el
        u0L = 0.5 * (uLminus + uLplus);
        rhs = basis(xiL) * (u0L - uLplus) * (-1); % normal is -1 (???)

        % Right face glift contrib
        uRminus = funkno(ielem, :) * basis(xiR);
        uRplus = funkno(elR_idx(ielem), :) * basis(xiL);
        u0R = 0.5 * (uRminus + uRplus);
        rhs = rhs + basis(xiR) * (u0R - uRminus) * (1);

        % calculate glift coeffs
        gliftcoeff = (mass_matrix \ rhs) / Jdet;

        for igauss = 1:size(gpoin, 1)
            % calcualte viscous flux
            u = funkno(ielem, :) * basis(gpoin(igauss));
            dudx = funkno(ielem, :) * dbasis(gpoin(igauss)) / Jdet;
            glift = gliftcoeff' * basis(gpoin(igauss));
            f = fvisc(u, dudx + glift);

            rescontrib = f * dbasis(gpoin(igauss)) * Jdet * weigh(igauss);
            % calculate and scatter to residual
            res(ielem, :) = res(ielem, :) + rescontrib';
        end
    end
end

