function [] = plotdg(fpoin, funkno)
%PLOTDG Plot a DG function assuming nodal basis functions
%   IN:
%    - fpoin = coordinates of points per element
%    - funkno = nodal values of the coordinates
    hold on
    ndegr = size(fpoin, 2);
    if(ndegr > 2)
        nsamplesel = ndegr * 2;
        for el = 1:size(fpoin, 1)
            samples = linspace(fpoin(el, 1), fpoin(el, end), nsamplesel);
            vsamples = interpn(fpoin(el, :), funkno(el, :), samples, 'cubic');
            plot(samples, vsamples, 'Color', [0 0.4470 0.7410]);
        end
    else
        for el = 1:size(fpoin, 1)
            plot(fpoin(el, :), funkno(el, :), 'Color', [0 0.4470 0.7410]);
        end
    end
end

