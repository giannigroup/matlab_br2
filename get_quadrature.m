function [gausspoin, weigh] = get_quadrature(ngauss)
%GET_QUADRATURE Get the quadrature rule information
% IN: 
%  -gauss the number of quadrature points to get
% OUT:
%  -gausspoin the quadrature points in the reference domain
%  -weigh the quadrature weights
    switch ngauss
        case 1
            gausspoin = 0;
            weigh = 2;
        case 2
            gausspoin = [-1 / sqrt(3); 1 / sqrt(3)];
            weigh = [1; 1];
        case 3
            gausspoin = [-sqrt(3 / 5); 0; sqrt(3 / 5)];
            weigh = [5/9; 8/9; 5/9];
        case 4
            gausspoin = [ ...
                -0.3399810435848563; ...
                0.3399810435848563;  ...
                -0.8611363115940526; ...
                0.8611363115940526   ...
            ];

            weigh = [ ...
                0.6521451548625461; ...
                0.6521451548625461; ...
                0.3478548451374538; ...
                0.3478548451374538  ...
            ];
    end
end

